package com.erostrum;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class TestBase {


    @BeforeClass(alwaysRun = true)
    public void setUP(){
        TestBase.setBaseURI("https://reqres.in");
    }


    public static void setBaseURI(String baseURI){
        RestAssured.baseURI=baseURI;
    }

}
